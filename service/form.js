const { dialog } = require('electron').remote;
const sharp = require('sharp');
const fs = require('fs');
$(document).ready(() => {
    $('#form-box').submit(function (e) {
        e.preventDefault()
        let args = {};
        $("#form-box :input").each(function (e) {
            var input = $(this); // This is the jquery object of the input, do what you will
            if (input.val().length > 0) {
                args = { ...args, [input.attr('name')]: input.val() }
            }
        });

        fs.readdir(args.filesDir, (err, files) => {
            files.forEach(file => {
                console.log(args.filesDir + file)
                sharp(args.filesDir + '/' + file).resize(parseInt(args.width), parseInt(args.heigth)).toFile(args.saveDir + '/' +file);
            })
        })
        console.log(args)
        return false;
    })
    $('#inputFolder').click(function () {
        let options = { properties: ["openDirectory"] }
        dialog.showOpenDialog(options, (dir) => {
            $('#inputFolder :input').val(dir);
        })
    })
    $('#saveFolder').click(function () {
        let options = { properties: ["openDirectory"] }
        dialog.showOpenDialog(options, (dir) => {
            $('#saveFolder :input').val(dir);
        })
    })
})